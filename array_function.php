<?php
//Video 17 also associate too
//Array reverse function
$testArray = [39, 859, 238, 549, 1.3, 9490, 39, "hello", 3];

print_r($testArray);
echo "<br>";
echo "The Reverse Array is : ";
print_r(array_reverse($testArray));

echo "<hr>";

//Array_pop function (delete the array)

print_r("The delete value is : " . array_pop($testArray));
echo "<br>";
echo "The Array after deleting the object : <br>";
print_r($testArray);
echo "<br>";

//Associative Array call with the Key for finding Values

$age = array("Rahim" => 24, "Karim" => 22, "Abdul" => 73);

if (array_key_exists("Karim", $age)) {
    echo "Age of Mr. Karim is " . $age['Karim'] . "<br>";
}

echo "<hr>";

// Find the KEY=>VALUE from Associative Global Array, call by SuperGlobal Variable "$_GET" 
if (array_key_exists("Rahim", $_GET)) {
    echo $_GET('Rahim');
} else {
    echo "The doesn't Exist!";
}

echo "<hr>";

//array_flip 
$tstArray = array("Rahim" => 24, "Karim" => 22, "Abdul" => 73);
print_r($tstArray);
echo "<br>";
$outputArray = array_flip($tstArray);
print_r($outputArray);


//array_pad()

$input = array(12, 10, 9);
echo "<br>";
echo "<hr>";
$result = array_pad($input, 5, 0);
print_r($result);
// result is array(12, 10, 9, 0, 0)

$result = array_pad($input, -7, -1);
echo "<br>";
echo "<hr>";
// result is array(-1, -1, -1, -1, 12, 10, 9)
print_r($result);

$result = array_pad($input, 2, "noop");
// not padded
echo "<br>";
echo "<hr>";
print_r($result);


//Array_push()

$stack = array("orange", "banana");
array_push($stack, "apple", "raspberry");
echo "<br>";
echo "<hr>";
print_r($stack);


//Array_keys()

echo "<hr>";
echo "<br>";
$array = array(0 => 100, "color" => "red");
print_r(array_keys($array));
echo "<br>";
$array = array("blue", "red", "green", "blue", "blue");
print_r(array_keys($array, "blue"));
echo "<br>";
$array = array(
    "color" => array("blue", "red", "green"),
    "size"  => array("small", "medium", "large")
);
print_r(array_keys($array));
echo "<br>";
echo "<hr>";


#Array_sum()

$testArray = array("Rahim" => 24, "Karim" => 22, "Abdul" => 73);
print_r(array_sum($testArray));
echo "<br>";
echo "<hr>";

#current()
$transport = array("foot" => 34, "bike" => 23, "car" => 56, "plane" => 54);
$mode = current($transport); // $mode = 'foot';
$mode = next($transport);    // $mode = 'bike';
$mode = current($transport); // $mode = 'bike';
$mode = prev($transport);    // $mode = 'foot';
$mode = end($transport);     // $mode = 'plane';
$mode = current($transport); // $mode = 'plane';

$kvArray = each($transport);
echo "[" . $kvArray["key"] . "]=>" . $kvArray["value"];

echo str_repeat("<br>", 10);
// $arr = array();
// var_dump(current($arr)); // bool(false)

// $arr = array(array());
// var_dump(current($arr)); // array(0) { }


//compact()
echo "<br>";
echo "<hr>";

$x = 34;
$y = 64;
$radius = 10;
$transport = array("foot" => 34, "bike" => 23, "car" => 56, "plane" => 54);
$center = array('x', 'y', 'radius', 'transport');

$circle = compact($center);
function drawACircle($circle)
{
    echo "<pre>";
    var_dump($circle);
    echo "</pre>";
}
drawACircle($circle);

//array_replace_recursive
echo "<br>";
echo "<hr>";
echo "<br>";
$color1 = array("red", "green", "blue", array("red" . "green", "blue"));
$color2 = array("purple", "white", "yellow");

print_r(array_replace_recursive($color1, $color2));


//substr_compare
echo "<br>";
echo "<hr>";
echo "<br>";
echo substr_compare("abcde", "bc", 1, 2);
echo "<br>"; // 0
echo substr_compare("abcde", "de", -2, 2);
echo "<br>"; // 0
echo substr_compare("abcde", "bcg", 1, 2);
echo "<br>"; // 0
echo substr_compare("abcde", "BC", 1, 2, true);
echo "<br>"; // 0
echo substr_compare("abcde", "bc", 1, 3);
echo "<br>"; // 1
echo substr_compare("abcde", "cd", 1, 2);
echo "<br>"; // -1
echo substr_compare("abcde", "abc", 5, 1); // warning

echo "<br>";
echo "<hr>";
echo "<br>";

//array_shift
$stack = array("orange", "banana", "apple", "raspberry");
$fruit = array_shift($stack);
echo "<pre>";
print_r($stack);
echo "</pre>";
echo str_repeat("*", 50) . "<br/>";

//array_shift
$queue = [
    "orange",
    "banana"
];

array_unshift($queue, "apple", "raspberry");
echo "<pre>";
var_dump($queue);
echo "</pre>";
echo str_repeat("*", 50) . "<br/>";


//array_unique

$color = array("a" => "green", "red", "b" => "green", "blue", "red");

print_r($color);
echo "<br/>";
print_r(array_unique($color));
echo "<br/> <br/>";
echo str_repeat("*", 50) . "<br/><br/>";
