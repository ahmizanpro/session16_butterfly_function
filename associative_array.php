<?php

//associative array;
//foreach loop;


$transport = array("foot" => 34, "bike" => 23, "car" => 56, "plane" => 54, "bus" => 34);


foreach ($transport as $key => $value) {
    echo $key . " => " . $value . "<br/>";
}
echo "<br/>";
$nth = 2;
$count = 1;

foreach ($transport as $key => $value) {
    if ($count == $nth) {
        echo $key . " => " . $value . "<br/>";
    }
    $count++;
}

echo "<br/>";
echo "<hr/>";
$person = array(
    array("Name" => "Karim", "Age" => 23, "BloodGroup" => "O+"),
    array("Name" => "Rahim", "Age" => 33, "BloodGroup" => "O-"),
    array("Name" => "Shafiq", "Age" => 34, "BloodGroup" => "A+"),
    array("Name" => "Shizan", "Age" => 32, "BloodGroup" => "B+"),
    array("Name" => "Kiron", "Age" => 44, "BloodGroup" => "AB+"),
);

$who = "Shafiq";

for ($i = 0; $i < count($person); $i++) {
    if ($person[$i]["Name"] == $who) {
        echo "Name: " . $person[$i]['Name'] . "<br/>";
        echo "Age: " . $person[$i]['Age'] . "<br/>";
        echo "BloodGroup: " . $person[$i]['BloodGroup'] . "<br/>";
    }
}
