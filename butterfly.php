<?php

$n = $_GET['n'];
$symbol = $_GET['symbol'];

function printSymbols($totalSymbols)
{
    $r = rand(0, 255);
    $g = rand(0, 255);
    $b = rand(0, 255);
    $a = rand();
    for ($i = 1; $i <= $totalSymbols; $i++) {
        echo "<span style='color: rgba($r, $g, $b, $a);'>" . $GLOBALS['symbol'] . "</span>";
    }
} //end of printSymbols function

function printSpaces($totalSpaces)
{
    for ($i = 1; $i <= $totalSpaces; $i++) {
        echo "&nbsp;";
    }
} //end of printSpaces function


function drawButterly($n)
{
    echo "<pre>";

    for ($i = 1; $i < $n; $i++) {
        printSymbols($i);
        printSpaces(($n - $i) * 2);
        printSymbols($i);
        echo "<br>";
    }


    for ($i = $n - 1; $i >= 1; $i--) {
        printSymbols($i);
        printSpaces(($n - $i) * 2);
        printSymbols($i);
        echo "<br>";
    }
    echo "</pre>";
}

drawButterly($n);

for ($i = 1; $i <= 10; $i++) {
    drawButterly($n * $i);
}
